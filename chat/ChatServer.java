package chat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tuplespaces.TupleSpace;

/**
 * 
 * @author Jos� Magalh�es & Jo�o Reis
 * Class responsible for create or join to a ChatServer.
 */
public class ChatServer {
	private final int maxSize;
	private final TupleSpace ts;
	private final List<String> channels;
	
	/**
	 * Constructor responsible for create a new ChatServer 
	 * @param t tuple space that will store the information about this Chat
	 * @param channel capacity
	 * @param channelNames name of the channels to be created
	 */
	public ChatServer(TupleSpace t, int rows, String[] channelNames) {
		ts = t;
		this.maxSize = rows;
		String info[] = {"chatserver", Integer.toString(channelNames.length), Integer.toString(maxSize)};
		channels = new ArrayList<String> (Arrays.asList(channelNames));
		for(int i = 0; i < channels.size(); i++){
			TupleSemaphore.create(t,"SemEmptyCount", channels.get(i)+"_sem_empty",rows);
			TupleSemaphore.create(t,"Mutex", channels.get(i), 1);
			String tuple [] = {"channelbuffer", channels.get(i) , "0", "0"};
			String tuple3 [] = {"channelhistory", channels.get(i) ,"0", "-1"};
			String tuple2 [] = {"chatchannel", Integer.toString(i), channels.get(i)};
			ts.put(tuple);
			ts.put(tuple2);
			ts.put(tuple3);
		}
		ts.put(info);
	}

	/**
	 * Constructor responsible for join to an existent Chat
	 * @param t tuple space
	 */
	public ChatServer(TupleSpace t) {
		int numChannels;
		String info[] = {"chatserver",null, null};
		ts = t;
		info = ts.read(info);
		numChannels = Integer.parseInt(info[1]);
		maxSize = Integer.parseInt(info[2]);
		channels = new ArrayList<String> ();
		for(int i = 0; i < numChannels; i++){
			String tuple [] = {"chatchannel", Integer.toString(i), null};
			tuple = ts.read(tuple);
			channels.add(tuple[2]);
		}
	}

	public String[] getChannels() {
		String[] tmp = new String[channels.size()];
		return channels.toArray(tmp);
	}

	/**
	 * Write message to a channel
	 * @param channel 
	 * @param message message to be written
	 */
	public void writeMessage(String channel, String message) {
		
		TupleSemaphore.semWait(ts, "SemEmptyCount", channel+"_sem_empty");
		TupleSemaphore.semWait(ts, "Mutex", channel);
		String[] channeltuple = ts.get("channelbuffer", channel, null, null);
		int nextMsg = Integer.parseInt(channeltuple[2]);
		int channelListeners = Integer.parseInt(channeltuple[3]);
		if (channelListeners > 0) {
			String[] msgtuple = {"message", channel, channeltuple[2], message, Integer.toString(0), channeltuple[3]};
			ts.put(msgtuple);
		}

		String[] historytuple = ts.get("channelhistory", channel, null, null);
		int historyLow = Integer.parseInt(historytuple[2]);
		int historyHigh = Integer.parseInt(historytuple[3]);
		historyHigh++;
		historytuple[3] = Integer.toString(historyHigh);
		if ((historyHigh - historyLow + 1) > maxSize) {
			ts.get("historymessage", channel, historytuple[2], null);
			historyLow++;
			historytuple[2] = Integer.toString(historyLow);			
		}
		String[] historymsg = {"historymessage", channel, historytuple[3], message};
		ts.put(historymsg);
		ts.put(historytuple);
	
		nextMsg++;
		channeltuple[2] = Integer.toString(nextMsg);
		ts.put(channeltuple);
		TupleSemaphore.semSignal(ts, "Mutex", channel);
		if (channelListeners == 0)
			TupleSemaphore.semSignal(ts, "SemEmptyCount", channel+"_sem_empty");
	}
	
	/**
	 * Create a new listener for channel
	 * @param channel
	 * @return a new listener
	 */
	public  ChatListener openConnection(String channel) {

		String[] channeltuple = ts.get("channelbuffer", channel, null, null);
		int channelListeners = Integer.parseInt(channeltuple[3]);
		channelListeners++;
		channeltuple[3] = Integer.toString(channelListeners);
		
		String[] historytuple = ts.read("channelhistory", channel, null, null);
		int historyLow = Integer.parseInt(historytuple[2]);
		int historyHigh = Integer.parseInt(historytuple[3]);

		ArrayList<String> history = new ArrayList<String>();
		for (; historyLow <= historyHigh; historyLow++) {
			String[] msg = ts.read("historymessage", channel, Integer.toString(historyLow), null);
			history.add(msg[3]);
		}

		ts.put(channeltuple);
		return new ChatListener(ts, channel, Integer.parseInt(channeltuple[2]), history);
	}
}
