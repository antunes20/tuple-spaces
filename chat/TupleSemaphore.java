package chat;

import tuplespaces.TupleSpace;

public class TupleSemaphore {

	private static final int SEM_VALUE = 2;
	private static final int SEM_WAITING = 3;
	private static final int SEM_RELEASED = 4;
	
	// (type, name, value, waiting, released)
	public static void create(TupleSpace ts, String type, String name, int v) {
		ts.put(type, name, Integer.toString(v), Integer.toString(0), Integer.toString(0));
	}
	/*
	integer waiting, released
	boolean isfree
	p1:	removenote(�mutex�, isfree, waiting, released)
	p2:	if( isfree )
	p3:		postnote(�mutex�, false, waiting, released)
	p4:	else
	p5:		waiting = waiting + 1
	p6:		postnote(�mutex�, false, waiting, released)
	p7:		removenote(�permit�, waiting=)//will block! */	
	public static void semWait(TupleSpace ts, String type, String name) {
		String[] sem = ts.get(type, name, null, null, null);
		
		int value = Integer.parseInt(sem[SEM_VALUE]);
		int waiting = Integer.parseInt(sem[SEM_WAITING]);

		if (value > 0) {
			value--;
			sem[SEM_VALUE] = Integer.toString(value);
			ts.put(sem);
		}
		else {
			waiting++;
			sem[SEM_WAITING] = Integer.toString(waiting);
			ts.put(sem);
			ts.get(type+"_permit", name, Integer.toString(waiting));
		}
	}
	
	/*integer waiting, released
	boolean isfree
	q1:	removenote(�mutex�, isfree, waiting, released)
	q2:	if( isfree )
	q3:		postnote(�mutex�, true, waiting, released)
	q4:	else if( waiting == released )
	q5:		postnote(�mutex�, true, waiting, released)
	q6:	else
	q7:		released <-	released + 1
	q8:		postnote(�mutex�, false, waiting, released)
	q9:		postnote(�permit�, released)//will unlbock a waiter	*/
	public static void semSignal(TupleSpace ts, String type, String name) {
		String[] sem = ts.get(type, name, null, null, null);

		int value = Integer.parseInt(sem[SEM_VALUE]);
		int waiting = Integer.parseInt(sem[SEM_WAITING]);
		int released = Integer.parseInt(sem[SEM_RELEASED]);		
		
		if (value > 0 || waiting == released) {
			value++;
			sem[SEM_VALUE] = Integer.toString(value);
			ts.put(sem);
		}
		else {
			released++;
			sem[SEM_RELEASED] = Integer.toString(released);
			ts.put(sem);
			ts.put(type+"_permit", name, Integer.toString(released));
		}
		
	}
	
}
