package chat;

import java.util.List;

import tuplespaces.TupleSpace;


/**
 * Class that represents a Listener
 * @author Jos� Magalh�es & Jo�o Reis
 *
 */
public class ChatListener {
	private final TupleSpace ts;
	private final String channel;
	private int nextMessageNumber;
	private List<String> history;
	
	/**
	 * Create a new Listener
	 * @param t tuple space
	 * @param channel
	 * @param nextMessage the next message that this listener must read
	 */
	public ChatListener(TupleSpace t, String channel, int nextMessage){
		ts = t;
		this.nextMessageNumber = nextMessage;
		this.channel = channel;
		history = null;
	}
	/**
	 * Create a new listener with messages to be read in history 
	 * @param t tuple space 
	 * @param channel
	 * @param nextMessage the next message that this listener must read
	 * @param history the messages that listener must read before read nextMessage
	 */
	public ChatListener(TupleSpace t, String channel, int nextMessage, List<String> history){
		ts = t;
		this.nextMessageNumber = nextMessage;
		this.channel = channel;
		if (history.isEmpty())
			this.history = null;
		else
			this.history = history;
	}

	/**
	 * Get the next message to be read
	 * @return message read
	 */
	public String getNextMessage() {

		if (history != null) {
			String ret = history.remove(0);
			if (history.isEmpty())
				history = null;
			return ret;
		}
		boolean removed = false;
		String[] msg = ts.get("message", channel, Integer.toString(nextMessageNumber), null, null, null);
		nextMessageNumber++;
		int msgListeners = Integer.parseInt(msg[4]);
		int maxListeners = Integer.parseInt(msg[5]);
		msgListeners++;
		msg[4] = Integer.toString(msgListeners);
		
		if(msgListeners < maxListeners)
			ts.put(msg);
		else
			removed = true;
		if (removed)
			TupleSemaphore.semSignal(ts, "SemEmptyCount", channel+"_sem_empty");	
		return msg[3];
	}
	/**
	 * Remove a listener
	 */
	public void closeConnection() {
		
		boolean removed = false;
		String[] channeltuple = ts.get("channelbuffer", channel, null, null);
		int nextMsg = Integer.parseInt(channeltuple[2]);
		int channelListeners = Integer.parseInt(channeltuple[3]);
		channelListeners--;
		channeltuple[3] = Integer.toString(channelListeners);
		ts.put(channeltuple);
		
		for(; nextMessageNumber < nextMsg; nextMessageNumber++) {
			removed = false;
			String[] msg = ts.get("message", channel,  Integer.toString(nextMessageNumber), null, null, null);
			int msgListeners = Integer.parseInt(msg[4]);
			int maxListeners = Integer.parseInt(msg[5]);
			msgListeners++;
			msg[4] = Integer.toString(msgListeners);

			if(msgListeners < maxListeners)
				ts.put(msg);
			else {
				removed = true;
			}
			
			if (removed) {
				TupleSemaphore.semSignal(ts, "SemEmptyCount", channel+"_sem_empty");
			}
		}
	}
}
