package tuplespaces.tree;

public class MyBlockingTree<T> {
	private int height;
	
	private MyBlockingNode<T> root;
	
	public MyBlockingTree(int height) {
		if (height <= 0)
			throw new RuntimeException("MyBlockingTree can't have height equal or less than 0.");
		this.height = height;
		root = new MyBlockingNode<T>(0);
	}
	
	public T[] removeTuple(T... tuple) {
		if (tuple.length != height)
			throw new RuntimeException("Length of tuple doesn't match tree height");
		return root.fetchTuple(true, tuple); 
	}
	
	public T[] readTuple(T... tuple) {
		if (tuple.length != height)
			throw new RuntimeException("Length of tuple doesn't match tree height");
		return root.fetchTuple(false, tuple);
	}
	
	public void postTuple(T... tuple) {
		if (tuple.length != height)
			throw new RuntimeException("Length of tuple doesn't match tree height");
		root.insertTuple(tuple);
	}
	
	
}
