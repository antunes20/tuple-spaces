package tuplespaces.tree;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import tuplespaces.util.MyBinarySemaphore;

/* Node of Tree */
public class MyBlockingNode<T> {
	// Element of tuple associated with the node 
	private T data;
	
	// Number of tuple instances covered by this node (sum of counts of all child nodes)
	// If leaf node then it's the number of instances of a specific tuple
	private int count;
	
	// Mutex of this node
	private MyBinarySemaphore mutex;
	
	// Child nodes
	private HashMap<T,MyBlockingNode<T>> children;
	
	// Waiting get operations
	private List<MyWaiter<T>> waitingRemovals;
	
	// Waiting read operations
	private List<MyWaiter<T>> waitingReads;

	private void init(T data, int count) {
		mutex = new MyBinarySemaphore();
		children = new HashMap<T,MyBlockingNode<T>>();
		waitingRemovals = null;
		waitingReads = null;
		this.data = data;
		this.count = count;
	}

	public MyBlockingNode(T data, int count) {
		init(data, count);
	}

	public MyBlockingNode(int count) {
		init(null, count);
	}

	/*
	 * Method called for read and get operations
	 */
	public T[] fetchTuple(boolean isRemove, T... pattern) {
		if (pattern == null)
			throw new RuntimeException("Tuple can't be null");

		mutex.myWait();
		return auxFetchTuple(new ArrayList<T>(), 0, isRemove, false, pattern); //if exception, mutex is unlocked anyway
	}
	
	/*
	 * method that gets a child node (and creates it if it doesn't exist)
	 */
	private MyBlockingNode<T> getAndLockChildNode(int i, T... tuple) {
		MyBlockingNode<T> next = children.get(tuple[i]);

		if (next == null) {
			next = new MyBlockingNode<T>(tuple[i],0);
			next.mutex.myWait();
			children.put(tuple[i],next);
		}
		else
			next.mutex.myWait();
		
		return next;
	}
	
	/*
	 * method called when leaf node has been reached in a get/read operation
	 */
	private T[] leafNodeFetch(List<T> tupleSoFar, boolean isRemove, boolean hasNull, T... pattern) {
		if (count > 0) {
			if (isRemove)
				count--;
			@SuppressWarnings("unchecked")
			T[] asd = (T[]) Array.newInstance(pattern.getClass().getComponentType(), tupleSoFar.size());
			asd = tupleSoFar.toArray(asd);
			mutex.mySignal();
			return asd;
		}
		if (hasNull) {
			mutex.mySignal();
			return null;
		}

		return waitForTuple(isRemove, pattern);		
	}
	
	/* method that creates MyWaiter object and calls waitC()
	 * 
	 */
	private T[] waitForTuple(boolean isRemove, T... pattern) {

		MyWaiter<T> newNode = new MyWaiter<T>(pattern);
		if (isRemove) {
			if (waitingRemovals == null)
				waitingRemovals = new ArrayList<MyWaiter<T>>();
			waitingRemovals.add(newNode);
		}
		else {
			if (waitingReads == null)
				waitingReads = new ArrayList<MyWaiter<T>>();
			waitingReads.add(newNode);
		}
		
		mutex.mySignal();
		newNode.waitC();		
		return newNode.foundTuple;
	}
	
	/*
	 * Method that performs a tree search looking for a tuple to match with a pattern that contains
	 * one or more wildcards. This is different from the standard tree search because current nodes' mutexes
	 * are not unlocked before processing all child nodes
	 */
	private T[] wildcardFetch(List<T> tupleSoFar, int i, boolean isRemove, boolean hasNull, T... pattern) {
		MyBlockingNode<T> node;
		Iterator<Entry<T, MyBlockingNode<T>>> it = children.entrySet().iterator();
		
		while (it.hasNext()) {
			Entry<T, MyBlockingNode<T>> entry = it.next();		        
			node = entry.getValue();
			node.mutex.myWait();
			tupleSoFar.add(node.data);
			
			T[] ret = node.auxFetchTuple(tupleSoFar, i+1, isRemove, hasNull, pattern);
			if (ret != null) {
				if (isRemove) {
					count--;
					node.mutex.myWait();
					if (node.count <= 0 && node.children.size() == 0 
							&& (node.waitingReads == null || node.waitingReads.isEmpty()) 
							&& (node.waitingRemovals == null || node.waitingRemovals.isEmpty())) {
						it.remove();
					}
					node.mutex.mySignal();
				}
				mutex.mySignal();
				return ret;
			}
			else
				tupleSoFar.remove(tupleSoFar.size()-1);
		}
		return null;
	}
	
	/*
	 * Method that removes a node if conditions are met
	 */
	private void checkAndRemoveNode(MyBlockingNode<T> node, int i, T... tuple) {

		node.mutex.myWait();
		if (node.count <= 0 && node.children.size() == 0 
				&& (node.waitingReads == null || node.waitingReads.isEmpty()) 
				&& (node.waitingRemovals == null || node.waitingRemovals.isEmpty())) {
			children.remove(tuple[i]);
		}
		node.mutex.mySignal();
	}

	/*
	 * Auxiliary recursive method for read/get operations
	 * hasNull is true if a wildcard has been found until the current moment
	 * tupleSoFar is a part of a tuple that was found so far (tuple associated with current tree path)
	 */
	private T[] auxFetchTuple(List<T> tupleSoFar, int i, boolean isRemove, boolean hasNull, T... pattern) {
	
		if (i >= pattern.length) // if last element of pattern (leaf node)
			return leafNodeFetch(tupleSoFar, isRemove, hasNull, pattern);

		MyBlockingNode<T> next = null;

		if (pattern[i] == null && !hasNull) { // if first wildcard encountered
			hasNull = true;
			
			// search subtrees below for a tuple that matches this pattern
			T[] ret = wildcardFetch(tupleSoFar, i, isRemove, hasNull, pattern);
			if (ret != null)
				return ret;
			
			// no tuple found, block
			return waitForTuple(isRemove, pattern);
			
		}
		else {
			if (hasNull) { // if wildcard was previously encountered (i.e. this is a wildcardFetch)
				MyBlockingNode<T> node;
				if (pattern[i] == null) {
					// another wildcard found, wildcardFetch inside current wildcardFetch!
					T[] ret = wildcardFetch(tupleSoFar, i, isRemove, hasNull, pattern);
					if (ret != null)
						return ret;
				}
				else { // wildcard was found in a node above the current one but this element is not a wildcard
					node = children.get(pattern[i]);
					if (node != null) {
						node.mutex.myWait();
						tupleSoFar.add(node.data);
						// do not release mutex since this is still a wildcardFetch
						T[] ret = node.auxFetchTuple(tupleSoFar, i+1, isRemove, hasNull, pattern);
						if (ret != null) { // if tuple was found
							if (isRemove) {
								count--;
								checkAndRemoveNode(node, i, pattern);
							}
							mutex.mySignal();
							return ret;
						}
						else
							tupleSoFar.remove(tupleSoFar.size()-1);
					}
				}
				
				mutex.mySignal();
				return null;
			}
			else { // this is not a wildcardFetch and current element is also not a wildcard
				next = getAndLockChildNode(i, pattern);

				tupleSoFar.add(pattern[i]);
				mutex.mySignal(); // here we can unlock mutex since it's not a wildcardFetch
				T[] rettuple = next.auxFetchTuple(tupleSoFar, i+1, isRemove, hasNull, pattern);

				if (rettuple != null && isRemove) {
					mutex.myWait();
					count--;
					
					// get node again in case it was already replaced by a new one
					// we took dozens of hours to find out about a bug related to this 
					// (it happened once in 100 executions) but we found it!
					next = children.get(pattern[i]);
					if (next != null) {
						checkAndRemoveNode(next, i, pattern);
					}
					mutex.mySignal();
					return rettuple;
				}
				return rettuple;
			}
		}
	}

	/*
	 * Method called put operations
	 */
	public void insertTuple(T... tuple) {
		if (tuple == null)
			throw new RuntimeException("Tuple can't be null");

		for (T t : tuple) {
			if (t == null)
				throw new RuntimeException("Tuple can't have null values");
		}
		mutex.myWait();
		auxInsertTuple(0, tuple);
	}

	/*
	 * Auxiliary recursive method for put operations
	 */
	private void auxInsertTuple(int i, T... tuple) {

		// check if there are read operations to unblock at this node
		if (waitingReads != null) {
			MyWaiter<T> node;
			for (int j = 0; j < waitingReads.size(); j++) {
				node = waitingReads.get(j);
				if (matchTupleWithPattern(tuple, node.tuple)) {
					waitingReads.remove(j);
					j--;
					node.signalC(tuple);
				}
			}
		}

		// check if there are get operations to unblock at this node
		if (waitingRemovals != null) {
			for (MyWaiter<T> node : waitingRemovals) {
				if (matchTupleWithPattern(tuple, node.tuple)) {
					waitingRemovals.remove(node);
					node.signalC(tuple);
					mutex.mySignal();
					return;
				}
			}
		}

		count++;

		if (i >= tuple.length) { // last element of tuple, leaf node
			mutex.mySignal();
			return;
		}
		
		if (tuple[i] == null)
			throw new RuntimeException("trying to insert tuple with null");

		MyBlockingNode<T> next = getAndLockChildNode(i, tuple);

		mutex.mySignal(); // unlock mutex before going to the next node
		next.auxInsertTuple(i+1, tuple);
		return;
	}

	/*
	 * simple method to match tuples with patterns
	 */
	private boolean matchTupleWithPattern(T[] tup, T[] pat) {
		if (tup.length != pat.length)
			return false;

		for (int i = 0; i < tup.length; i++) {
			if (tup[i] == null)
				throw new RuntimeException("Tuple has null value!");
			if (pat[i] != null && !tup[i].equals(pat[i]))
				return false;
		}
		return true;
	}
}
