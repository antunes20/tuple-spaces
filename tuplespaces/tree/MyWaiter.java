package tuplespaces.tree;

public class MyWaiter<T> {
	public T[] tuple;
	public T[] foundTuple;
	private Object cond;
	private boolean condv;

	public MyWaiter(T... tuple) {
		this.tuple = tuple.clone();
		this.cond = new Object();
		condv = false;
		foundTuple = null;
	}

	public void waitC() {
		synchronized(cond) {
			while (!condv) {
				try {
					cond.wait();
				} catch (InterruptedException e) {	}
			}
		}
	}

	public void signalC(T... tuple) {
		synchronized(cond) {
			this.foundTuple = tuple.clone();
			condv = true;
			cond.notifyAll();
		}
	}
}
