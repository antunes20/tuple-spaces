package tuplespaces.util;

public class MyBinarySemaphore {

	private boolean free;
	
	public MyBinarySemaphore() {
		free = true;
	}
	
	public MyBinarySemaphore(boolean free) {
		this.free = free;
	}
	
	public synchronized void myWait() {
		while(!free) {
			try {
				this.wait();
			} catch (InterruptedException e) {}			
		}
		free = false;
	}
	
	public synchronized void mySignal() {
		free = true;
		this.notify();
	}
}
