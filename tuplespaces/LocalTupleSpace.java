package tuplespaces;

import java.util.HashMap;

import tuplespaces.tree.MyBlockingTree;

public class LocalTupleSpace implements TupleSpace {
	private Object mapLock;
	private HashMap<Integer,MyBlockingTree<String>> myMap;
	
	public LocalTupleSpace() {
		mapLock = new Object();
		myMap = new HashMap<Integer,MyBlockingTree<String>>();
	}
	
	public String[] get(String... pattern) {
		pattern = pattern.clone();
		MyBlockingTree<String> tree;
		synchronized(mapLock) {
			tree = myMap.get(pattern.length);
			if (tree == null) {
				tree = new MyBlockingTree<String>(pattern.length);
				myMap.put(pattern.length, tree);
			}
		}
		return tree.removeTuple(pattern);
	}

	public String[] read(String... pattern) {
		pattern = pattern.clone();
		MyBlockingTree<String> tree;
		synchronized(mapLock) {
			tree = myMap.get(pattern.length);
			if (tree == null) {
				tree = new MyBlockingTree<String>(pattern.length);
				myMap.put(pattern.length, tree);
			}
		}
		return tree.readTuple(pattern);
	}

	public void put(String... tuple) {
		tuple = tuple.clone();
		MyBlockingTree<String> tree;
		synchronized(mapLock) {
			tree = myMap.get(tuple.length);
			if (tree == null) {
				tree = new MyBlockingTree<String>(tuple.length);
				myMap.put(tuple.length, tree);
			}
		}
		tree.postTuple(tuple);
	}
}
